python-keystonemiddleware (10.9.0-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-binary-memcached as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 26 Feb 2025 13:16:57 +0100

python-keystonemiddleware (10.7.1-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090523).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 10:20:40 +0100

python-keystonemiddleware (10.7.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 17:06:07 +0200

python-keystonemiddleware (10.7.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Sep 2024 12:07:48 +0200

python-keystonemiddleware (10.7.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 11:05:11 +0200

python-keystonemiddleware (10.6.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:17:46 +0200

python-keystonemiddleware (10.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-jwt as (b-)depends.
  * Drop do-not-use-utcnow.patch.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Feb 2024 17:27:01 +0100

python-keystonemiddleware (10.4.1-3) unstable; urgency=medium

  * Add do-not-use-utcnow.patch (Closes: #1058254).

 -- Thomas Goirand <zigo@debian.org>  Thu, 14 Dec 2023 08:29:07 +0100

python-keystonemiddleware (10.4.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * d/watch: use version 4 and gitmode.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Oct 2023 09:07:44 +0200

python-keystonemiddleware (10.4.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (b-)depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 31 Aug 2023 10:37:54 +0200

python-keystonemiddleware (10.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Blacklist tests expecting keystone on port 5000.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Aug 2023 16:26:46 +0200

python-keystonemiddleware (10.2.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1046350).

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Aug 2023 00:02:38 +0200

python-keystonemiddleware (10.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 10:43:57 +0200

python-keystonemiddleware (10.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed applied upstream patch:
    CVE-2022-2447_Remove_cache_invalidation_when_using_expired_token.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 13:32:42 +0100

python-keystonemiddleware (10.1.0-4) unstable; urgency=medium

  * Add CVE-2022-2447_Remove_cache_invalidation_when_using_expired_token.patch
    (Closes: #1021272).

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Oct 2022 21:21:10 +0200

python-keystonemiddleware (10.1.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 09:27:35 +0200

python-keystonemiddleware (10.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 Sep 2022 16:59:20 +0200

python-keystonemiddleware (10.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 31 Aug 2022 09:15:06 +0200

python-keystonemiddleware (10.0.1-1) experimental; urgency=medium

  [ Mohammed Bilal ]
  * Added autopkgtests

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Aug 2022 08:42:53 +0200

python-keystonemiddleware (9.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 13:14:32 +0100

python-keystonemiddleware (9.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Feb 2022 17:08:22 +0100

python-keystonemiddleware (9.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 10:32:38 +0200

python-keystonemiddleware (9.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed switch-to-eventlet-oslo-cache-memcacheclientpool.patch applied
    upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Aug 2021 11:00:31 +0200

python-keystonemiddleware (9.2.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 08:47:56 +0200

python-keystonemiddleware (9.2.0-2) experimental; urgency=medium

  * d/copyright: Add me to copyright file
  * d/control: Add me to uploaders field
  * d/patches: Add switch-to-eventlet-oslo-cache-memcacheclientpool.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Sat, 26 Jun 2021 13:43:00 +0200

python-keystonemiddleware (9.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 10:16:49 +0100

python-keystonemiddleware (9.1.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 10:38:33 +0200

python-keystonemiddleware (9.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Sep 2020 15:34:05 +0200

python-keystonemiddleware (9.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 13:39:32 +0200

python-keystonemiddleware (9.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-sphinxcontrib.svg2pdfconverter as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Apr 2020 19:05:27 +0200

python-keystonemiddleware (7.0.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 22:00:46 +0200

python-keystonemiddleware (7.0.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 27 Sep 2019 11:41:58 +0200

python-keystonemiddleware (6.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Jul 2019 23:32:23 +0200

python-keystonemiddleware (6.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Thu, 28 Mar 2019 10:49:56 +0100

python-keystonemiddleware (5.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Sep 2018 23:04:04 +0200

python-keystonemiddleware (5.2.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Refreshed no-intersphinx patch.
  * Fixed (build-)depends for this release.
  * Building sphinx doc with Python 3.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 16:36:11 +0200

python-keystonemiddleware (4.21.0-0) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed re-add-missing-auth-options.patch: will hard-wire this in a
    pkgos-readd-keystone-authtoken-options script.

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 Mar 2018 13:39:03 +0100

python-keystonemiddleware (4.17.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Nov 2017 23:57:37 +0000

python-keystonemiddleware (4.17.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.
  * Rebase patches.
  * Blacklist more failing unit tests.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2017 21:02:57 +0200

python-keystonemiddleware (4.4.0-3) unstable; urgency=medium

  * Re-add missing auth options in oslo-config-generator:
    - Add re-add-missing-auth-options.patch
    - Disable now failing unit tests.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Apr 2016 22:16:03 +0000

python-keystonemiddleware (4.4.0-2) unstable; urgency=medium

  * Added git as build-depends-indep.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 11:22:51 +0000

python-keystonemiddleware (4.4.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Uploading to unstable.
  * Fixed (build-)depends for this release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 12:21:37 +0200

python-keystonemiddleware (4.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Also test with Python 3.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Dec 2015 16:29:42 +0100

python-keystonemiddleware (3.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Dec 2015 11:02:00 +0100

python-keystonemiddleware (2.3.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 10:04:17 +0000

python-keystonemiddleware (2.3.0-2) experimental; urgency=medium

  * Added Python 3 support.

 -- Thomas Goirand <zigo@debian.org>  Sat, 03 Oct 2015 19:48:25 +0200

python-keystonemiddleware (2.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Align dependencies with upstream.
  * d/control: Update uploaders.

 -- Corey Bryant <corey.bryant@canonical.com>  Wed, 30 Sep 2015 14:42:41 -0400

python-keystonemiddleware (2.1.0-2) experimental; urgency=medium

  * Removed python-bandit build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 20:50:50 +0000

python-keystonemiddleware (2.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed watch file.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 07:38:14 +0000

python-keystonemiddleware (1.5.0-2) unstable; urgency=high

  * CVE-2015-1852: S3Token TLS cert verification option not honored. Applied
    upstream patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jun 2015 08:28:00 +0000

python-keystonemiddleware (1.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed nature.css from debian/copyright (and it's BSD licence).

 -- Thomas Goirand <zigo@debian.org>  Wed, 08 Apr 2015 10:08:46 +0200

python-keystonemiddleware (1.0.0-3) unstable; urgency=medium

  * Added CVE-2014-7144_convert_the_conf_value_into_correct_type.patch. Thanks
    to Luciano Bello <luciano@debian.org> for the report (Closes: #762748).

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Sep 2014 07:16:29 +0000

python-keystonemiddleware (1.0.0-2) unstable; urgency=medium

  * Do not attempt to run unit tests in Python 2.6, as it needs the discover
    package, which we don't want as build-depends.
  * Removes intersphinx plugin from docs build.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Jul 2014 00:29:44 +0800

python-keystonemiddleware (1.0.0-1) unstable; urgency=medium

  * Initial release. (Closes: #755135)

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Jul 2014 14:25:47 +0800
